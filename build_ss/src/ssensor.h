#ifndef _SS_H
#define _SS_H

int open_ss(unsigned int address);
static int write_ss_reg_address( uint8_t u8_Address );
int read_ss( uint8_t u8_Reg_Address, uint8_t *buf, uint8_t u8_Len );
int get_number_sensors(uint8_t *n_sensors);
int get_sensor_type( uint8_t u8_N_Sensor, uint8_t *p_type );
int get_measurement( uint8_t u8_N_Sensor, float *p_measurment );
int get_units_of_measure( uint8_t u8_N_Sensor, uint8_t *uom );
int write_ss( uint8_t, uint8_t *, uint8_t );
#define SS_ADDRESS 0x68

#define TEMP_SENSOR_PRESENT      ( 1 << 1)
#define HUMIDITY_SENSOR_PRESENT  ( 1 << 2)
#define BARO_SENSOR_PRESENT      ( 1 << 3)

#define INTERRUPT_CONTROL_REGISTER  0x18
#define CONTROL_REGISTER            0x14
#define EVENT1_TIMEBASE_REGISTER    0x10

#endif
