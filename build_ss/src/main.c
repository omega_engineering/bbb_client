#include <poll.h>

#include <anjay/anjay.h>
#include <anjay/security.h>
#include <anjay/server.h>

#include <avsystem/commons/time.h>

#include "ssensor.h"

static int setup_security_object(anjay_t *anjay) {
    const anjay_security_instance_t security_instance = {
        .ssid = 1,
        .server_uri = "coap://192.168.1.103:5683",
        .security_mode = ANJAY_UDP_SECURITY_NOSEC
    };

    if (anjay_security_object_install(anjay)) {
        return -1;
    }

    // let Anjay assign an Object Instance ID
    anjay_iid_t security_instance_id = ANJAY_IID_INVALID;
    if (anjay_security_object_add_instance(anjay, &security_instance,
                                           &security_instance_id)) {
        return -1;
    }

    return 0;
}

static int setup_server_object(anjay_t *anjay) {
    const anjay_server_instance_t server_instance = {
        .ssid = 1,
        .lifetime = 86400,
        .default_min_period = 5,
        .default_max_period = 10,
        .disable_timeout = -1,
        .binding = ANJAY_BINDING_U
    };

    if (anjay_server_object_install(anjay)) {
        return -1;
    }

    anjay_iid_t server_instance_id = ANJAY_IID_INVALID;
    if (anjay_server_object_add_instance(anjay, &server_instance,
                                         &server_instance_id)) {
        return -1;
    }

    return 0;
}

int main_loop(anjay_t *anjay) {
    while (true) {
        // Obtain all network data sources
        AVS_LIST(avs_net_abstract_socket_t *const) sockets =
                anjay_get_sockets(anjay);

        // Prepare to poll() on them
        size_t numsocks = AVS_LIST_SIZE(sockets);
        struct pollfd pollfds[numsocks];
        size_t i = 0;
        AVS_LIST(avs_net_abstract_socket_t *const) sock;
        AVS_LIST_FOREACH(sock, sockets) {
            pollfds[i].fd = *(const int *) avs_net_socket_get_system(*sock);
            pollfds[i].events = POLLIN;
            pollfds[i].revents = 0;
            ++i;
        }

        const int max_wait_time_ms = 1000;
        // Determine the expected time to the next job in milliseconds.
        // If there is no job we will wait till something arrives for
        // at most 1 second (i.e. max_wait_time_ms).
        int wait_ms =
                anjay_sched_calculate_wait_time_ms(anjay, max_wait_time_ms);

        // Wait for the events if necessary, and handle them.
        if (poll(pollfds, numsocks, wait_ms) > 0) {
            int socket_id = 0;
            AVS_LIST(avs_net_abstract_socket_t *const) socket = NULL;
            AVS_LIST_FOREACH(socket, sockets) {
                if (pollfds[socket_id].revents) {
                    anjay_serve(anjay, *socket);
                }
                ++socket_id;
            }
        }

        // Finally run the scheduler (ignoring its return value, which
        // is the number of tasks executed)
        (void) anjay_sched_run(anjay);
    }
    return 0;
}

anjay_dm_object_def_t *p_get_pressure_object(void);
anjay_dm_object_def_t *p_get_humidity_object(void);
anjay_dm_object_def_t *p_get_temperature_object(void);

int main(argc, argv) 
int argc;
char **argv;
{
    int status;
    uint8_t number_sensors;
    uint8_t u8_sensor_type;
    uint8_t u8_n;
    uint8_t u8_sensors_present = 0;
    char *client_name;

   if( argc > 1 )
   {
      client_name = argv[1];
   }
   else
   {
      client_name = "bbb_client";
   }

   anjay_configuration_t CONFIG = {
        .endpoint_name = (const char *)client_name, //"ssensor_client_1",
        .in_buffer_size = 4000,
        .out_buffer_size = 4000
    };

    anjay_t *anjay = anjay_new(&CONFIG);
    if (!anjay) {
        return -1;
    }

    int result = 0;

    if(setup_security_object(anjay)
            || setup_server_object(anjay)) {
        result = -1;
        goto cleanup;
    }

    status = open_ss( SS_ADDRESS );
    if( status < 0 )
    {
      fprintf( stderr, "Can't open smart sensor\n" );
      goto cleanup;
    }

    status = get_number_sensors( &number_sensors );

    fprintf( stderr, "Number of sensors found  = %d\n", number_sensors );

    // retrieve types of sensors
    for( u8_n = 0; u8_n < number_sensors; u8_n++ )
    {
      status = get_sensor_type( u8_n,  &u8_sensor_type );
      u8_sensors_present |= 1 << u8_sensor_type;
      fprintf(stderr, "sensor %d is type %d\n", u8_n, u8_sensor_type );
    }

    // initialize and register the pressure object

    // note: in this simple case the object does not have any state,
    // so it's fine to use a plain double pointer to its definition struct

    anjay_dm_object_def_t *pressure_object_def_ptr = p_get_pressure_object();
    anjay_dm_object_def_t *humidity_object_def_ptr = p_get_humidity_object();
    anjay_dm_object_def_t *temperature_object_def_ptr = p_get_temperature_object();

    if( u8_sensors_present & BARO_SENSOR_PRESENT )
    {
      anjay_register_object(anjay, &pressure_object_def_ptr);
    }

    if( u8_sensors_present & HUMIDITY_SENSOR_PRESENT )
    {
       anjay_register_object(anjay, &humidity_object_def_ptr);
    }

    if( u8_sensors_present & TEMP_SENSOR_PRESENT )
    {
        anjay_register_object(anjay, &temperature_object_def_ptr);
    }

    result = main_loop(anjay);

cleanup:
    anjay_delete(anjay);

    // pressure object does not need cleanup
    return result;
}

