#include <poll.h>

#include <anjay/anjay.h>
#include <anjay/security.h>
#include <anjay/server.h>

#include <avsystem/commons/time.h>

#include "ssensor.h"

static anjay_dm_attributes_t attributes = { 10, 15 };

static int attribute_handler( anjay_t *anjay, const anjay_dm_object_def_t *const *obj_ptr, anjay_ssid_t ssid,
                             anjay_dm_attributes_t *out )
{
  out = &attributes;
  return 0;
}

static int pressure_resource_read(anjay_t *anjay,
                              const anjay_dm_object_def_t *const *obj_ptr,
                              anjay_iid_t iid,
                              anjay_rid_t rid,
                              anjay_output_ctx_t *ctx) 
{
    // These arguments may seem superfluous now, but they will come in handy
    // while defining more complex objects
    (void) anjay;   // unused
    (void) obj_ptr; // unused: the object holds no state
    (void) iid;     // unused: will always be 0 for single-instance Objects

    float value;
    int status;

    switch (rid) {
    case 0:  // Label
        return anjay_ret_string(ctx, "Barometer");

    case 1:  // Timestamp
        return anjay_ret_i64(ctx, avs_time_real_now().since_real_epoch.seconds);

    case 2:  // Current value
        status = get_measurement( 2, &value );   // TODO: sensor number is hard wired, need to set during enumeration
        return anjay_ret_float( ctx, value );
    default:
        // control will never reach this part due to object's supported_rids
        return 0;
    }
}

static const anjay_dm_object_def_t OBJECT_DEF = 
{
    // Object ID
    .oid = 3315,

    // List of supported Resource IDs
    .supported_rids = ANJAY_DM_SUPPORTED_RIDS(0, 1, 2),

    .handlers = {
        // single-instance Objects can use these pre-implemented handlers:
        .instance_it = anjay_dm_instance_it_SINGLE,
        .instance_present = anjay_dm_instance_present_SINGLE,

        .object_read_default_attrs = attribute_handler,

        // if all supported Resources are always available, one can use
        // a pre-implemented `resource_present` handler too:
        .resource_present = anjay_dm_resource_present_TRUE,

        .resource_read = pressure_resource_read

        // all other handlers can be left NULL if only Read operation is required
    }
};

const anjay_dm_object_def_t *p_get_pressure_object(void)
{
  return &OBJECT_DEF;
}
