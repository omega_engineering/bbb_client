#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

#include "ssensor.h"

static int fd;

int get_number_sensors( uint8_t *p_n_sensors )
{
	int status;

	status = read_ss( 0x1a, p_n_sensors, 1 );

	return status;

}

int get_sensor_type( uint8_t u8_N_Sensor, uint8_t *p_type )
{
	int status;
	uint8_t u8_Address = 0x60 + 8 * u8_N_Sensor;

	status = read_ss( u8_Address, p_type, 1 );

	return status;
}

int get_measurement( uint8_t u8_N_Sensor, float *p_measurement )
{
	int status;
	uint8_t u8_Address = 0x3c + 4 * u8_N_Sensor;
	uint8_t measurement[4];

	status = read_ss( u8_Address, &measurement[0], 4 );

	((uint8_t *)p_measurement)[0] = measurement[3];
	((uint8_t *)p_measurement)[1] = measurement[2];
	((uint8_t *)p_measurement)[2] = measurement[1];
	((uint8_t *)p_measurement)[3] = measurement[0];

}

int get_units_of_measure( uint8_t u8_N_Sensor, uint8_t *uom )
{
	int status;
	uint8_t u8_Address = 0x64 + 8 * u8_N_Sensor;

	status = read_ss( u8_Address, uom, 4 );
}

int open_ss(unsigned int address)
{
int status;
uint16_t u16_irq_control = (1 << 5 );
uint16_t u16_event1_timebase = 1;
uint16_t u16_system_control = (1 << 15) | (1 << 14) | (1 << 11) | (1 << 4) | (1 << 5) | 0xf;

        u16_irq_control = htons( u16_irq_control );
        u16_event1_timebase = htons(  u16_event1_timebase );
        u16_system_control = htons( u16_system_control );

	const char *device_name = "/dev/i2c-2";
	fd = open( device_name, O_RDWR );
	if( fd > 0 )
	{
		if( ioctl( fd, I2C_SLAVE, address ) < 0 )
		{
			return -1;
		}

                status = write_ss( INTERRUPT_CONTROL_REGISTER, (uint8_t *)&u16_irq_control, 2 );
                status = write_ss( EVENT1_TIMEBASE_REGISTER, (uint8_t *)&u16_event1_timebase, 2 );
                status = write_ss( CONTROL_REGISTER, (uint8_t *)&u16_system_control, 2 );
	}

	return fd > 0 ? 0 : -1;

}

static int write_ss_reg_address( uint8_t u8_Address )
{
	int n_written;

	n_written = write( fd, &u8_Address, 1 );

	return n_written == 1 ? 0 : -1;

}

int read_ss( uint8_t u8_Reg_Address, uint8_t *buf, uint8_t u8_Len )
{
	int n_read = 0;
	int status = write_ss_reg_address( u8_Reg_Address );

	if( !status )
	{
		n_read = read( fd, buf, u8_Len );
	}


	return n_read == u8_Len ? 0 : -1;
}


#define WRITE_BUF_SIZE 100
static uint8_t u8_write_buffer[WRITE_BUF_SIZE];

int write_ss( uint8_t u8_Reg_Address, uint8_t *data, uint8_t len )
{
int n_written;

   memset(u8_write_buffer, 0, WRITE_BUF_SIZE );

   memcpy(u8_write_buffer, &u8_Reg_Address, 1 );
   memcpy(u8_write_buffer+1, data, len );

   n_written = write( fd, u8_write_buffer, len+1 );

   fprintf("length written = %d\n", n_written );

   return 0;

}
